package com.wheel.support.feature.ScheduleActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.wheel.support.R;
import com.wheel.support.common.model.EngineersResponseModel;
import com.wheel.support.common.model.SelectedEngineersModel;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by aravinda on 11/11/19.
 */

public class ScheduleActivity extends MvpActivity<ScheduleView, SchedulePresenter> implements ScheduleView {

    @BindView(R.id.header_back_button)
    ImageButton backButton;

    @BindView(R.id.header_text)
    TextView headerText;

    @BindView(R.id.schedule_table)
    TableLayout scheduleTableView;

    private ArrayList<EngineersResponseModel.EngineersData> engineersDataArrayList;
    private ArrayList<SelectedEngineersModel.DayShift> selectedEngineersPerDayList;
    private ArrayList<SelectedEngineersModel.ScheduleDays> scheduleDaysList;
    private ArrayList<SelectedEngineersModel> finalSelectedEngineersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);

        initialiseUIData();
    }

    private void initialiseUIData() {
        backButton.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getText(R.string.header_schedule));

        Bundle args = getIntent().getBundleExtra("BUNDLE");
        engineersDataArrayList = (ArrayList<EngineersResponseModel.EngineersData>) args.getSerializable("engineers_list");
        Log.d("list", "" + engineersDataArrayList.size());

        finalSelectedEngineersList = fetchRandomList();

        //DateTime today = new DateTime().withTimeAtStartOfDay();
        DateTime today = new DateTime().withDate(2019, 11, 11);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd - MM - yyyy");
        for (int dates = 0; dates < 14; dates++) {
            TableRow datesRow = new TableRow(this);
            TextView date = new TextView(this);
            date.setText(formatter.print(today.plusDays(dates)));
            date.setTextColor(Color.BLACK);
            date.setGravity(Gravity.CENTER);
            date.setPadding(6, 18, 6, 18);
            datesRow.addView(date);

            TextView dayShiftView = new TextView(this);
            dayShiftView.setText(finalSelectedEngineersList.get(dates).getScheduleDaysList().get(0).getDayShiftsList().get(0).getEngineerName()
                    + ", " + finalSelectedEngineersList.get(dates).getScheduleDaysList().get(0).getDayShiftsList().get(1).getEngineerName());
            dayShiftView.setTextColor(Color.BLACK);
            dayShiftView.setGravity(Gravity.CENTER);
            dayShiftView.setPadding(6, 18, 6, 18);
            datesRow.addView(dayShiftView);

            TextView nightShiftView = new TextView(this);
            nightShiftView.setText(finalSelectedEngineersList.get(dates).getScheduleDaysList().get(0).getNightShiftsList().get(0).getEngineerName()
                    + ", " + finalSelectedEngineersList.get(dates).getScheduleDaysList().get(0).getNightShiftsList().get(1).getEngineerName());
            nightShiftView.setTextColor(Color.BLACK);
            nightShiftView.setGravity(Gravity.CENTER);
            nightShiftView.setPadding(6, 18, 6, 18);
            datesRow.addView(nightShiftView);

            scheduleTableView.addView(datesRow);
        }
    }

    private ArrayList<SelectedEngineersModel> fetchRandomList() {
        ArrayList<EngineersResponseModel.EngineersData> randomEngineersPerDayList = new ArrayList<>();

        SelectedEngineersModel.DayShift selectedEngineersPerDay;
        SelectedEngineersModel.NightShift nightShift;
        SelectedEngineersModel.ScheduleDays scheduleDays;
        SelectedEngineersModel engineersModel;
        ArrayList<SelectedEngineersModel> finalEngineersList = new ArrayList<>();
        ArrayList<SelectedEngineersModel.NightShift> nightShiftArrayList;

        for (int i = 0; i < 14; i++) {
            selectedEngineersPerDayList = new ArrayList<>();
            nightShiftArrayList = new ArrayList<>();
            scheduleDaysList = new ArrayList<>();
            scheduleDays = new SelectedEngineersModel.ScheduleDays();
            engineersModel = new SelectedEngineersModel();
            if (i == 0) {
                randomEngineersPerDayList = getRandomEngineersForDay(4);

                for (int x = 0; x < randomEngineersPerDayList.size(); x++) {
                    if (x < 2) {
                        selectedEngineersPerDay = new SelectedEngineersModel.DayShift();
                        selectedEngineersPerDay.setEngineerId(randomEngineersPerDayList.get(x).getEmployeeId());
                        selectedEngineersPerDay.setEngineerName(randomEngineersPerDayList.get(x).getEmployeeName());
                        selectedEngineersPerDayList.add(selectedEngineersPerDay);
                    } else if (x >= 2) {
                        nightShift = new SelectedEngineersModel.NightShift();
                        nightShift.setEngineerId(randomEngineersPerDayList.get(x).getEmployeeId());
                        nightShift.setEngineerName(randomEngineersPerDayList.get(x).getEmployeeName());
                        nightShiftArrayList.add(nightShift);
                    }

                }
                scheduleDays.setDayShiftsList(selectedEngineersPerDayList);
                scheduleDays.setNightShiftsList(nightShiftArrayList);
                scheduleDaysList.add(scheduleDays);
                engineersModel.setScheduleDaysList(scheduleDaysList);
                finalEngineersList.add(engineersModel);
                Log.d("list", "" + finalEngineersList.size());
            } else {
                if (i > 2) {
                    for (int a = 0; a < randomEngineersPerDayList.size(); a++) {
                        if (a < 4) {
                            randomEngineersPerDayList.remove(a);
                            break;
                        }
                    }
                }
                randomEngineersPerDayList = getNextListOfEngineers(randomEngineersPerDayList, 4);

                for (int x = 0; x < randomEngineersPerDayList.size(); x++) {
                    if (x < 2) {
                        selectedEngineersPerDay = new SelectedEngineersModel.DayShift();
                        selectedEngineersPerDay.setEngineerId(randomEngineersPerDayList.get(x).getEmployeeId());
                        selectedEngineersPerDay.setEngineerName(randomEngineersPerDayList.get(x).getEmployeeName());
                        selectedEngineersPerDayList.add(selectedEngineersPerDay);
                    } else if (x >= 2) {
                        nightShift = new SelectedEngineersModel.NightShift();
                        nightShift.setEngineerId(randomEngineersPerDayList.get(x).getEmployeeId());
                        nightShift.setEngineerName(randomEngineersPerDayList.get(x).getEmployeeName());
                        nightShiftArrayList.add(nightShift);
                    }
                }
                scheduleDays.setDayShiftsList(selectedEngineersPerDayList);
                scheduleDays.setNightShiftsList(nightShiftArrayList);
                scheduleDaysList.add(scheduleDays);
                engineersModel.setScheduleDaysList(scheduleDaysList);
                finalEngineersList.add(engineersModel);
                Log.d("list", "" + finalEngineersList.size());
            }
        }
        return finalEngineersList;
    }

    private ArrayList<EngineersResponseModel.EngineersData> getNextListOfEngineers(List<EngineersResponseModel.EngineersData> selectedEngineersList, int maxEngineers) {
        Random rand = new Random();
        ArrayList<EngineersResponseModel.EngineersData> newList = new ArrayList<>();
        ArrayList<EngineersResponseModel.EngineersData> totalEngineersDataList = new ArrayList<>(engineersDataArrayList);

        int randomIndex;
        for (int i = 0; i < maxEngineers; i++) {
            randomIndex = rand.nextInt(totalEngineersDataList.size());
            for (EngineersResponseModel.EngineersData engineersData : selectedEngineersList) {
                if (!totalEngineersDataList.get(randomIndex).getEmployeeName().equals(engineersData.getEmployeeName())) {
                    newList.add(totalEngineersDataList.get(randomIndex));
                    totalEngineersDataList.remove(randomIndex);
                    break;
                }
            }
        }
        return newList;
    }

    public ArrayList<EngineersResponseModel.EngineersData> getRandomEngineersForDay(int maxEngineers) {
        Random rand = new Random();
        ArrayList<EngineersResponseModel.EngineersData> newList = new ArrayList<>();
        ArrayList<EngineersResponseModel.EngineersData> engineersDataList = new ArrayList<>(engineersDataArrayList);

        for (int i = 0; i < maxEngineers; i++) {
            int randomIndex = rand.nextInt(engineersDataList.size());
            newList.add(engineersDataList.get(randomIndex));
            engineersDataList.remove(randomIndex);
        }
        return newList;
    }

    @OnClick(R.id.header_back_button)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_back_button:
                finish();
                break;
        }
    }

    @Override
    public SchedulePresenter createPresenter() {
        return new SchedulePresenter();
    }
}
