package com.wheel.support.feature.HomeActivity;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.wheel.support.common.model.EngineersResponseModel;

/**
 * Created by aravinda on 11/11/19.
 */

public interface HomeView extends MvpView {

    void showLoader();

    void hideLoader();

    void showMessageToUser(String message);

    void updateEngineersData(EngineersResponseModel engineersResponseModel);
}
