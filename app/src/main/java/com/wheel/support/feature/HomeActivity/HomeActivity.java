package com.wheel.support.feature.HomeActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.wheel.support.R;
import com.wheel.support.common.adapter.EngineersListAdapter;
import com.wheel.support.common.model.EngineersResponseModel;
import com.wheel.support.feature.ScheduleActivity.ScheduleActivity;
import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;

public class HomeActivity extends MvpActivity<HomeView, HomePresenter> implements HomeView {

    @BindView(R.id.engineers_recyclerview)
    RecyclerView engineersRecyclerView;

    private SpotsDialog spotsDialog;
    private EngineersListAdapter engineersListAdapter;
    private ArrayList<EngineersResponseModel.EngineersData> engineersDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initialiseUIData();
    }

    private void initialiseUIData() {
        //fetching engineers data
        presenter.getEngineersData();

        //initialising & setting the adapter
        engineersDataArrayList = new ArrayList<>();
        engineersListAdapter = new EngineersListAdapter(HomeActivity.this, engineersDataArrayList);
        engineersRecyclerView.setAdapter(engineersListAdapter);
        engineersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.generate_schedule_button)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.generate_schedule_button:
                Intent intent = new Intent(HomeActivity.this, ScheduleActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("engineers_list", (Serializable) engineersDataArrayList);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
                break;
        }
    }

    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    public void hideLoader() {
        if (spotsDialog != null) {
            spotsDialog.dismiss();
        }
    }

    @Override
    public void showLoader() {
        spotsDialog = new SpotsDialog(this, "Loading Data....", R.style.Custom);
        spotsDialog.show();
    }

    @Override
    public void showMessageToUser(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateEngineersData(EngineersResponseModel engineersResponseModel) {
        //updating the engineers list
        engineersDataArrayList.clear();
        engineersDataArrayList.addAll(engineersResponseModel.getEngineersData());
        engineersListAdapter.notifyDataSetChanged();
    }
}
