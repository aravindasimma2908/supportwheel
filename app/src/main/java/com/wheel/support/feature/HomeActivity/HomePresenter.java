package com.wheel.support.feature.HomeActivity;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.wheel.support.common.model.EngineersResponseModel;
import com.wheel.support.common.rest.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aravinda on 11/11/19.
 */

public class HomePresenter extends MvpBasePresenter<HomeView> {

    public void getEngineersData() {
        if (isViewAttached())
            getView().showLoader();

        Call<EngineersResponseModel> call = new NetworkService().networkAPI.getEngineersData();
        call.enqueue(new Callback<EngineersResponseModel>() {
            @Override
            public void onResponse(Call<EngineersResponseModel> call, Response<EngineersResponseModel> response) {
                if (isViewAttached())
                    getView().hideLoader();
                if (response.code() == 200) {
                    try {
                        if (isViewAttached())
                            getView().updateEngineersData(response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (isViewAttached())
                        getView().showMessageToUser("Something went wrong, please try again!");
                }
            }

            @Override
            public void onFailure(Call<EngineersResponseModel> call, Throwable t) {
                if (isViewAttached()) {
                    getView().hideLoader();
                    getView().showMessageToUser("Please check your internet connection");
                }
            }
        });
    }
}
