package com.wheel.support.common.model;

import java.util.ArrayList;

public class SelectedEngineersModel {

    private ArrayList<ScheduleDays> scheduleDaysList;

    public ArrayList<ScheduleDays> getScheduleDaysList() {
        return scheduleDaysList;
    }

    public void setScheduleDaysList(ArrayList<ScheduleDays> scheduleDaysList) {
        this.scheduleDaysList = scheduleDaysList;
    }

    public static class ScheduleDays {

        private ArrayList<DayShift> dayShiftsList;
        private ArrayList<NightShift> nightShiftsList;

        public ArrayList<DayShift> getDayShiftsList() {
            return dayShiftsList;
        }

        public void setDayShiftsList(ArrayList<DayShift> dayShiftsList) {
            this.dayShiftsList = dayShiftsList;
        }

        public ArrayList<NightShift> getNightShiftsList() {
            return nightShiftsList;
        }

        public void setNightShiftsList(ArrayList<NightShift> nightShiftsList) {
            this.nightShiftsList = nightShiftsList;
        }
    }

    public static class DayShift {
        private int engineerId;

        private String engineerName;

        public int getEngineerId() {
            return engineerId;
        }

        public String getEngineerName() {
            return engineerName;
        }

        public void setEngineerId(int engineerId) {
            this.engineerId = engineerId;
        }

        public void setEngineerName(String engineerName) {
            this.engineerName = engineerName;
        }
    }

    public static class NightShift {
        private int engineerId;

        private String engineerName;

        public int getEngineerId() {
            return engineerId;
        }

        public String getEngineerName() {
            return engineerName;
        }

        public void setEngineerId(int engineerId) {
            this.engineerId = engineerId;
        }

        public void setEngineerName(String engineerName) {
            this.engineerName = engineerName;
        }
    }

}
