package com.wheel.support.common.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aravinda on 11/11/19.
 */

public class NetworkService {

    public static NetworkAPI networkAPI;
    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static String URL = "http://private-69a5e-engineerslist1.apiary-mock.com";

    public NetworkService() {
        this(URL);
    }

    private NetworkService(String baseUrl) {
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60,TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }
}
