package com.wheel.support.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EngineersResponseModel implements Serializable {

    @SerializedName("engineers")
    @Expose
    private ArrayList<EngineersData> engineersData;

    public ArrayList<EngineersData> getEngineersData() {
        return engineersData;
    }

    public static class EngineersData implements Serializable{

        @SerializedName("id")
        @Expose
        private int employeeId;

        @SerializedName("name")
        @Expose
        private String employeeName;

        @Override
        public boolean equals(Object v) {
            boolean retVal = false;

            if (v instanceof EngineersData){
                EngineersData engineersData = (EngineersData) v;
                retVal = (engineersData.employeeName.equals(this.employeeName));
            }

            return retVal;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 17 * hash + (this.employeeName != null ? this.employeeName.hashCode() : 0);
            return hash;
        }

        public int getEmployeeId() {
            return employeeId;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeId(int employeeId) {
            this.employeeId = employeeId;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }
    }
}
