package com.wheel.support.common.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wheel.support.R;
import com.wheel.support.common.model.EngineersResponseModel;

import java.util.ArrayList;

/**
 * Created by aravinda on 11/11/19.
 */

public class EngineersListAdapter extends RecyclerView.Adapter<EngineersListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<EngineersResponseModel.EngineersData> engineersDataArrayList;

    public EngineersListAdapter(Context context,
                                ArrayList<EngineersResponseModel.EngineersData> engineersDataArrayList) {
        this.context = context;
        this.engineersDataArrayList = engineersDataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_row_item, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.employeeName.setText(engineersDataArrayList.get(position).getEmployeeName());
    }

    @Override
    public int getItemCount() {
        return engineersDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView employeeName;

        public ViewHolder(View itemView) {
            super(itemView);

            employeeName = itemView.findViewById(R.id.employee_name);
        }
    }
}
