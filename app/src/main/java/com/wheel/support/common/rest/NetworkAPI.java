package com.wheel.support.common.rest;

import com.wheel.support.common.model.EngineersResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by aravinda on 11/11/19.
 */

public interface NetworkAPI {

    @GET("engineers")
    Call<EngineersResponseModel> getEngineersData();

}
